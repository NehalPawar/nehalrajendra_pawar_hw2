/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation;

import baseclasses.CpuCore;
import baseclasses.InstructionBase;
import baseclasses.PipelineRegister;
import baseclasses.PipelineStageBase;
import implementation.AllMyLatches.DecodeToExecute;
import implementation.AllMyLatches.ExecuteToMemory;
import implementation.AllMyLatches.FetchToDecode;
import implementation.AllMyLatches.MemoryToWriteback;
import utilitytypes.EnumComparison;
import utilitytypes.EnumOpcode;
import utilitytypes.Operand;
import voidtypes.VoidLatch;

/**
 * The AllMyStages class merely collects together all of the pipeline stage 
 * classes into one place.  You are free to split them out into top-level
 * classes.
 * 
 * Each inner class here implements the logic for a pipeline stage.
 * 
 * It is recommended that the compute methods be idempotent.  This means
 * that if compute is called multiple times in a clock cycle, it should
 * compute the same output for the same input.
 * 
 * How might we make updating the program counter idempotent?
 * 
 * @author
 */
public class AllMyStages {
    /*** Fetch Stage ***/
    static class Fetch extends PipelineStageBase<VoidLatch,FetchToDecode> {
        public Fetch(CpuCore core, PipelineRegister input, PipelineRegister output) {
            super(core, input, output);
        }
        
        @Override
        public String getStatus() {
            // Generate a string that helps you debug.
            return null;
        }

        @Override
        public void compute(VoidLatch input, FetchToDecode output) {
            GlobalData globals = (GlobalData)core.getGlobalResources();
            int pc = globals.program_counter;
            // Fetch the instruction
            InstructionBase ins = globals.program.getInstructionAt(pc);
            if (ins.isNull()) return;

            // Do something idempotent to compute the next program counter.
            
            // Don't forget branches, which MUST be resolved in the Decode
            // stage.  You will make use of global resources to commmunicate
            // between stages.
            
            // Your code goes here...
           
            	output.setInstruction(ins); 
        
        }
        
        @Override
        public boolean stageWaitingOnResource() {
            // Hint:  You will need to implement this for when branches
            // are being resolved.
       /* if(output_reg.isSlaveStalled())
        		return true;
        	else*/
            return false;
        }
        
        
        /**
         * This function is to advance state to the next clock cycle and
         * can be applied to any data that must be updated but which is
         * not stored in a pipeline register.
         */
        @Override
        public void advanceClock() {
            // Hint:  You will need to implement this help with waiting
            // for branch resolution and updating the program counter.
            // Don't forget to check for stall conditions, such as when
            // nextStageCanAcceptWork() returns false.
        	 GlobalData globals = (GlobalData)core.getGlobalResources();
        	 if (nextStageCanAcceptWork()) {
             	
        		 output_reg.setMasterBubble(false);
        		
        		 globals.program_counter++;
 			}
        	
        	 
        }
    }

    
    /*** Decode Stage ***/
    static class Decode extends PipelineStageBase<FetchToDecode,DecodeToExecute> {
        public Decode(CpuCore core, PipelineRegister input, PipelineRegister output) {
            super(core, input, output);
        }
        
        @Override
        public boolean stageWaitingOnResource() {
            // Hint:  You will need to implement this to deal with 
            // dependencies.
        	if(input_reg.isSlaveStalled())
        		return true;
        	else
            return false;
        }
        

        @Override
        public void compute(FetchToDecode input, DecodeToExecute output) {
            InstructionBase ins = input.getInstruction();
            
            // You're going to want to do something like this:
            
            // VVVVV LOOK AT THIS VVVVV
            ins = ins.duplicate();
            // ^^^^^ LOOK AT THIS ^^^^^
            
            // The above will allow you to do things like look up register 
            // values for operands in the instruction and set them but avoid 
            // altering the input latch if you're in a stall condition.
            // The point is that every time you enter this method, you want
            // the instruction and other contents of the input latch to be
            // in their original state, unaffected by whatever you did 
            // in this method when there was a stall condition.
            // By cloning the instruction, you can alter it however you
            // want, and if this stage is stalled, the duplicate gets thrown
            // away without affecting the original.  This helps with 
            // idempotency.
            
            //System.out.println("Decode " +ins);
            
            // These null instruction checks are mostly just to speed up
            // the simulation.  The Void types were created so that null
            // checks can be almost completely avoided.
            if (ins.isNull()) return;
            
           
            	
            GlobalData globals = (GlobalData)core.getGlobalResources();
            int[] regfile = globals.register_file;
            
            // Do what the decode stage does:
            // - Look up source operands
            // - Decode instruction
            // - Resolve branches  
            //stage waiting on reesourct if()
            /*if(stageWaitingOnResource()==true) {
            	
            }*/
          
            globals.StallNow=false;
            input_reg.setSlaveStall(false);
           
            if(globals.BRA_FLag==true){
            	globals.BRA_FLag=false;
            	return;
            }
            if(input.getInstruction().getSrc2().isRegister()==true) {
    			if(globals.register_invalid[ins.getSrc2().getRegisterNumber()]==true)   {     				
    				input_reg.setSlaveStall(true);    				
    			globals.StallNow=true;
    			for(int i=1;i<4;i++) {
    				if(core.getForwardingDestinationRegisterNumber(i)==input.getInstruction().getSrc2().getRegisterNumber())
    				//if(core.isForwardingResultValid(i)){
    					input_reg.setSlaveStall(false);
    					globals.StallNow=false;
    					break;
    				//}
    			}   
    			}
    		}
           
    		if(input.getInstruction().getSrc1().isRegister()==true) {
        		if (globals.register_invalid[ins.getSrc1().getRegisterNumber()]==true) { 
        			input_reg.setSlaveStall(true);
        			globals.StallNow=true;
        			
        			for(int i=1;i<4;i++) {
        				if(core.getForwardingDestinationRegisterNumber(i)==input.getInstruction().getSrc1().getRegisterNumber()) {     					
        					      						
        						if(output_reg.read().getInstruction().getOpcode()==EnumOpcode.ADD)
        						output_reg.read().getInstruction().getOper0().setValid(true);
        					if(core.isForwardingResultValid(i)){        					
		        					input_reg.setSlaveStall(false);
		        					globals.StallNow=false;
		        					
		        					break;
        						}        					
        				}
        			}     
        		}
            }
    		if(!ins.getOpcode().needsWriteback())
    		if(input.getInstruction().getOper0().isRegister()==true)
    			if(globals.register_invalid[ins.getOper0().getRegisterNumber()]==true) {
        		input_reg.setSlaveStall(true);
        		globals.StallNow=true;
        		if(ins.getOpcode().equals(EnumOpcode.STORE))
            		for(int i=1;i<4;i++) {
        				if(core.getForwardingDestinationRegisterNumber(i)==ins.getSrc1().getRegisterNumber()) {
        					//if(core.isForwardingResultValid(i)) {        					 
          					input_reg.setSlaveStall(false);
        					globals.StallNow=false;
        					break;
        					//}
        				}
        			} 
        		if(ins.getOpcode().equals(EnumOpcode.BRA))
        		for(int i=1;i<4;i++) {
    				if(core.getForwardingDestinationRegisterNumber(i)==ins.getOper0().getRegisterNumber()) {
    					if(core.isForwardingResultValid(i)) {
    					 
      					input_reg.setSlaveStall(false);
    					globals.StallNow=false;
    					break;
    					}
    				}
    			}  
    		}
      	
            if(input.getInstruction().getOper0().isRegister()==true) { 
            	Operand op=ins.getOper0();            	
            	op.setValue(globals.register_file[ins.getOper0().getRegisterNumber()]);
            	ins.setOper0(op);
            	
            	}
           if (input.getInstruction().getSrc1().isRegister()==true) {            	
            	Operand op=ins.getSrc1();            	
            	op.setValue(globals.register_file[ins.getSrc1().getRegisterNumber()]);
            	ins.setSrc1(op);
			} 

            if (input.getInstruction().getSrc2().isRegister()==true) {
            	Operand op=ins.getSrc2();
            	op.setValue(globals.register_file[ins.getSrc2().getRegisterNumber()]);
            	ins.setSrc2(op);
			}
            int result=0;
            if(globals.StallNow==false)
            if (ins.getOpcode().equals(EnumOpcode.BRA)) {
            	input_reg.setSlaveStall(true);
				globals.StallNow=true;
            	for(int i=1;i<4;i++) {
    				if(core.getForwardingDestinationRegisterNumber(i)==ins.getOper0().getRegisterNumber()) {
    					if(core.isForwardingResultValid(i)) {
    					  Operand op=ins.getOper0();            	
    					  op.setValue(core.getForwardingResultValue(i));
      					ins.setOper0(op);
      					input_reg.setSlaveStall(false);
    					globals.StallNow=false;
    					break;
    					}
    				}
    			} 
				result=MyALU.executeBRA_INS(ins.getOper0(), ins.getLabelTarget(),ins.getComparison());
				
				if(result!=-1) {
				globals.program_counter=(result-1);
				
				globals.BRA_FLag=true;
								
				return;
				}
			
			}
            else if(ins.getOpcode().equals(EnumOpcode.JMP)){
            		globals.program_counter=ins.getLabelTarget().getAddress()-1;
            		globals.BRA_FLag=true;
            		
 				return;
            		}
         
            

            if(!input_reg.isSlaveStalled() ){
            	output.setInstruction(ins);	            	
            	if(input.getInstruction().getOper0().isRegister())    
            		if(input.getInstruction().getOpcode()!=EnumOpcode.STORE) {
            	globals.register_invalid[ins.getOper0().getRegisterNumber()]=true;
            	
            		}
            }           
            
       
            // Set other data that's passed to the next stage.
        }
    }
    

    /*** Execute Stage ***/
    static class Execute extends PipelineStageBase<DecodeToExecute,ExecuteToMemory> {
        public Execute(CpuCore core, PipelineRegister input, PipelineRegister output) {
            super(core, input, output);
        }
       /* @Override
        public boolean stageWaitingOnResource() {
            // Hint:  You will need to implement this to deal with 
            // dependencies.
        	if(input_reg.isSlaveStalled()) {
        		input_reg.setSlaveStall(false);
        		return true;
        	}
        	else
            return false;
        }*/
        @Override
        public void compute(DecodeToExecute input, ExecuteToMemory output) {
            InstructionBase ins = input.getInstruction();
            GlobalData globals = (GlobalData)core.getGlobalResources();
            //System.out.println("execute" +ins);
            if (ins.isNull()) return;
            
            
            Operand src1 =ins.getSrc1(); 
            Operand src2 =ins.getSrc2();
            
            	if(ins.getOpcode().needsWriteback()||ins.getOpcode()==EnumOpcode.STORE)//retrive value from next memory in here and assing to R that want to read
            		
            		{
            			for(int i=2;i<4;i++) {            			
            				 if(input.getInstruction().getSrc1().isRegister()==true)
            				if(core.getForwardingDestinationRegisterNumber(i)==input.getInstruction().getSrc1().getRegisterNumber()) {
            					src1.setValue(core.getForwardingResultValue(i));
            					ins.setSrc1(src1);
            					
            				}
            				 if(input.getInstruction().getSrc2().isRegister()==true)
                 				if(core.getForwardingDestinationRegisterNumber(i)==input.getInstruction().getSrc2().getRegisterNumber()) {
                 					src2.setValue(core.getForwardingResultValue(i));
                 					ins.setSrc2(src2);
                 					
                 				}
            			}   
				}
				
            	int source1 = ins.getSrc1().getValue();//either it will set new value or teh default value
                int source2 = ins.getSrc2().getValue();
                int oper0 =   ins.getOper0().getValue();
               
                int result;
            
             result= MyALU.execute(ins.getOpcode(), source1, source2, oper0);
           
            output.opertemp=result;          
         
            // Fill output with what passes to Memory stage...
            
            if(ins.getOpcode().needsWriteback()) {//write in ins the new result to desigantion
            Operand op=ins.getOper0();            	
        	op.setValue(result);
        	ins.setOper0(op);
        	
        	ins.getOper0().setValid(true);
        	
            }
           
				
			
           
            if(ins.getOpcode()==EnumOpcode.OUT)
            {
            	int count=0;
            	count++;
            }
            
           
            output.setInstruction(ins);
            // Set other data that's passed to the next stage.
        }
    }
    

    /*** Memory Stage ***/
    static class Memory extends PipelineStageBase<ExecuteToMemory,MemoryToWriteback> {
        public Memory(CpuCore core, PipelineRegister input, PipelineRegister output) {
            super(core, input, output);
        }

        @Override
        public void compute(ExecuteToMemory input, MemoryToWriteback output) {
            InstructionBase ins = input.getInstruction();
            GlobalData globals= (GlobalData)core.getGlobalResources();
           
            if (ins.isNull()) return;
            
           if(ins.getOpcode().equals(EnumOpcode.STORE)){
        	  
        	   globals.Memory_Block[input.opertemp]=ins.getOper0().getValue();//ins.getSrc1().getValue()]= // globals.register_file[ins.getOper0().getRegisterNumber()];
        	   
           }
           else if(ins.getOpcode().equals(EnumOpcode.LOAD)){
        	   for(int i=3;i<4;i++) {         
        		   Operand src1 =ins.getSrc1(); 
    				 if(input.getInstruction().getSrc1().isRegister()==true)
    				if(core.getForwardingDestinationRegisterNumber(i)==input.getInstruction().getSrc1().getRegisterNumber()) {
    					src1.setValue(core.getForwardingResultValue(i));
    					ins.setSrc1(src1);  //give the value to ins so that in case of instruction left pipeline it will take value from default
    					break;
    				}    				
    			}  
        	   
        	   Operand op=ins.getOper0();            	
           	op.setValue(globals.Memory_Block[ins.getSrc1().getValue()]);
           	ins.setOper0(op);
           	ins.getOper0().setValid(true);
        	   output.setInstruction(ins);
           }
           else
        	   output.setInstruction(ins);
            // Access memory...
                    
          
            // Set other data that's passed to the next stage.
          
        }
    }
    

    /*** Writeback Stage ***/
    static class Writeback extends PipelineStageBase<MemoryToWriteback,VoidLatch> {
        public Writeback(CpuCore core, PipelineRegister input, PipelineRegister output) {
            super(core, input, output);
        }

        @Override
        public void compute(MemoryToWriteback input, VoidLatch output) {
            InstructionBase ins = input.getInstruction();
            
            GlobalData globals = (GlobalData)core.getGlobalResources();
                    
            
            if (ins.isNull()) return;
            
            // Write back result to register file
           
            
            if(input.getInstruction().getOper0().isRegister()==true) 
            globals.register_file[ins.getOper0().getRegisterNumber()]=ins.getOper0().getValue();
            if(input.getInstruction().getOper0().isRegister()==true) {            	
               	globals.register_invalid[ins.getOper0().getRegisterNumber()]=false;
            }
            if(ins.getOpcode().equals(EnumOpcode.LOAD)) {
           	   
            	
           	   globals.register_file[ins.getOper0().getRegisterNumber()]=ins.getOper0().getValue();//globals.Memory_Block[ins.getSrc1().getValue()];
           	  
           	   if(input.getInstruction().getOper0().isRegister()==true) {            	
                    	globals.register_invalid[ins.getOper0().getRegisterNumber()]=false;
                 }
              }
           
            if (input.getInstruction().getOpcode() == EnumOpcode.OUT)
            	System.out.println("@@output:"+globals.Memory_Block[ins.getOper0().getValue()]);
            if (input.getInstruction().getOpcode() == EnumOpcode.HALT) {
                // Stop the simulation
            	System.exit(0);
            }
        }
    }
}
